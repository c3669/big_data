from pywebhdfs.webhdfs import PyWebHdfsClient
from hdfs import InsecureClient
from PIL import Image
import cv2
import os
from pathlib import Path
import time

# Использовать from hdfs import InsecureClient


root_path = '/'  # Корень HDFS

client_pyweb = PyWebHdfsClient(user_name='asemenov', port=9870, host='127.0.0.1')
client_hdfs = InsecureClient(url="http://127.0.0.1:9870", user='asemenov', root=root_path)


def upload_data(local_path: str, type_file: str, name_file: str, chunk_size: int = 128, hdfs_pdth: str = None):
    if type_file == 'image':
        with Image.open(local_path) as data:
            start_time = time.time()
            client_pyweb.create_file(os.path.join(hdfs_pdth, name_file), data, blocksize=chunk_size, overwrtie=True)
            return time.time() - start_time

    elif type_file == 'video':
        with cv2.VideoCapture(local_path) as data:
            start_time = time.time()
            client_pyweb.create_file(os.path.join(hdfs_pdth, name_file), data, blocksize=chunk_size, overwrtie=True)
            return time.time() - start_time


def download_data(local_path: str, type_file: str, name_file: str, hdfs_pdth: str = None):
    start_time = time.time()
    data = client_pyweb.read_file(hdfs_pdth)
    time_result = time.time() - start_time()

    if type_file == 'image':
        cv2.imwrite(os.path.join(local_path, name_file), data)
    elif type_file == 'video':
        cv2.VideoWriter(os.path.join(local_path, name_file)).write(data)

    return time_result


time_upload = upload_data()

# time_download = download_data

# def delete_data():
#
#
# def create_dir():
#
#
# def delete_dir():


# image = Image.open("files_example/man.jpg")
# image.show()

# print(hdfs.get_file_dir_status('/hadoop_test.txt'))
# with Image.open("./files_example/man.jpg") as file_data:
#     # file_data.show()
#     hdfs.create_file('user/root/man.jpg', file_data, blocksize=64, overwrtie=True)
